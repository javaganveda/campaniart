
public class Artist {
	
	private String idArtist;
	private String name; 
    private String surname;  
    private int    birthYear;
	private String biography;
	
	
	public String getIdArtist() {
		return idArtist;
	}
	public void setIdArtist(String idArtist) {
		this.idArtist = idArtist;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	
	
	
}
