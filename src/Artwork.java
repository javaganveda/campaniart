
public class Artwork {

	private String	id;
	private String	name;
	private int 	age;
	private String 	city; 
	private String 	address;
	private int 	civicNumber;
	private double 	xSat;
	private double 	ySat;
	private String idArtist;
	private double 	value;
	private int tipology;
	private double height;
	private double width;
	private double weight;
	private double lenght;
	private String description;
	private String idPhoto;
	private boolean deleted;
	
	
	public int getTipology() {
		return tipology;
	}
	public void setTipology(int tipology) {
		this.tipology = tipology;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public int getCivicNumber() {
		return civicNumber;
	}
	public void setCivicNumber(int civicNumber) {
		this.civicNumber = civicNumber;
	}
	public double getxSat() {
		return xSat;
	}
	public void setxSat(double xSat) {
		this.xSat = xSat;
	}
	public double getySat() {
		return ySat;
	}
	public void setySat(double ySat) {
		this.ySat = ySat;
	}
	public String getIdArtist() {
		return idArtist;
	}
	public void setIdArtist(String idArtist) {
		this.idArtist = idArtist;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getLenght() {
		return lenght;
	}
	public void setLenght(double lenght) {
		this.lenght = lenght;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIdPhoto() {
		return idPhoto;
	}
	public void setIdPhoto(String idPhoto) {
		this.idPhoto = idPhoto;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	
}
