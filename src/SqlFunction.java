import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlFunction {
	
	private static final String URL = "jdbc:mysql://127.0.0.1:3306/campaniart";
	private static final String USER = "root";
	private static final String PASSWORD = "Protom2016";
	
	
	public static void create_schema(){
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE SCHEMA `campaniart`";
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create schema
	
	public static void createTable(){
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `campaniart`.`artwork`(`id` VARCHAR(45) NOT NULL,`name` VARCHAR(45),`age` INT NULL,`city` VARCHAR(45),"
					+ "`address` VARCHAR(45),`civicNumber` INT,`xSat` DOUBLE NULL, `ySat` DOUBLE NULL,`idArtist` VARCHAR(45),"
					+ "`value` DOUBLE NULL, tipology INT,"
					+ "`height` DOUBLE NULL,"
					+ "`width` DOUBLE NULL,"
					+ "`weight` DOUBLE NULL,"
					+ "`lenght` DOUBLE NULL,"
					+ "`description` VARCHAR(100) NULL,"
					+ "`deleted` TINYINT,`idPhoto` VARCHAR(45),"
					+ "PRIMARY KEY (`id`))";
			

			statement.executeUpdate(sql);
			
			sql = "CREATE TABLE `campaniart`.`artist`(`idartist` VARCHAR(45) NOT NULL,`name` VARCHAR(45),`surname` VARCHAR(45),`birthyear` INT,"
					+ "`biography` VARCHAR(200) NULL,"
					+ "PRIMARY KEY (`idartist`))";
			

			statement.executeUpdate(sql);
	

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create table
}
