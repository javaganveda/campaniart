import java.util.Scanner;

public class GuiCampaniArt {

	static MySqlArtworkRepository artworkSql = new MySqlArtworkRepository(); 
	static Artwork artwork = new Artwork();
	
	public static void startMenu(){

	try (Scanner keyPressed = new Scanner(System.in)){
			
		/* crea l�oggetto che rappresenta la tastiera */	
		System.out.println("|-------------------------------|"); //istruzione per stampare a video 
		System.out.println("|------------Men�---------------|");
		System.out.println("|-------------------------------|"); //istruzione per stampare a video 
		System.out.println("|1 - Choose category            |");
		System.out.println("|2 - Quick search               |");
		System.out.println("|3 - Option                     |");
		System.out.println("|0 - Exit                       |");
		System.out.println("|-------------------------------|");
		System.out.println("|-------------------------------|"); //istruzione per stampare a video 
		System.out.println("|-------------------------------|"); //istruzione per stampare a video 

	do{
	int keyInt = keyPressed.nextInt();
		
	switch(keyInt)
	{
	case 1:
	categoryMenu();
	break;

	case 2:
	// Ricerca Veloce		
	break;
	
	case 3:
	optionMenu();
	break;
	
	case 0:
	System.exit(1);
	break;
	
	default:
	System.out.println("Comando non trovato");
	System.out.println("");
	break;
	} 	
	}while(keyPressed.hasNextInt()); //fine while key  pressed
	}	
}//FINE METODO
	
	static public void categoryMenu(){
		
		try (Scanner keyPressed = new Scanner(System.in)){
		do{
			System.out.println("|----------------------------|"); //istruzione per stampare a video 
			System.out.println("|------------Artwork---------|");
			System.out.println("|----------------------------|"); //istruzione per stampare a video 
			System.out.println("|1 - Statue                  |");
			System.out.println("|2 - Painting                |");
			System.out.println("|3 - Building                |");
			System.out.println("|4 - Artefact                |");
			System.out.println("|5 - back                    |");
			System.out.println("|----------------------------|");
			
			int keyInt = keyPressed.nextInt();
					
		switch(keyInt){
		
		case 1:
			statueMenu();
			break;
		
		case 2:
			paintingMenu();
			break;	
			
		case 3:
			buildingMenu();
			break;
		
		case 4:
			artefactMenu();
			break;
	
		case 5:
			startMenu();
			break;
			
		default:
			System.out.println("Choose correct Artwork...");
			break;
		}
		break;
		}while(keyPressed.hasNextInt());
	}	
	}
	
	
static public void optionMenu(){
		
		try (Scanner keyPressed = new Scanner(System.in)){

			
		do{
			System.out.println("|----------------------------|"); //istruzione per stampare a video 
			System.out.println("|------------Options---------|");
			System.out.println("|----------------------------|"); //istruzione per stampare a video 
			System.out.println("|1 - Create Scheme           |");
			System.out.println("|2 - Create Table            |");
			System.out.println("|3 - Delete Scheme           |");
			System.out.println("|4 - Delete Table            |");
			System.out.println("|5 - back                    |");
			System.out.println("|----------------------------|");		
		
			int keyInt = keyPressed.nextInt();

			
		switch(keyInt){
		
		case 1:
			
				SqlFunction.create_schema();
				
				System.out.println("Press Any Key To Continue...");
		          new java.util.Scanner(System.in).nextLine();
			
			optionMenu();
			break;
		
		case 2:
	
			SqlFunction.createTable();
			System.out.println("Press Any Key To Continue...");
	          new java.util.Scanner(System.in).nextLine();
			optionMenu();

			break;	
			
		case 3:
			
		

			break;
		
		case 4:
	

			break;
			
		case 5:
			startMenu();
			break;
	
		default:
			System.out.println("Choose correct Artwork...");
			break;
		}	
	}while(keyPressed.hasNextInt()); }	
	}
	
static public void statueMenu(){
	
	String keyPressedString;
	
	try (Scanner keyPressed = new Scanner(System.in)){

	
	do{
	
		System.out.println("|--------STATUE MENU---------|"); //istruzione per stampare a video 
		System.out.println("|----------------------------|");
		System.out.println("|0 - List                    |"); //istruzione per stampare a video 
		System.out.println("|1 - View                    |");
		System.out.println("|2 - Insert                  |");
		System.out.println("|3 - Update                  |");
		System.out.println("|4 - Delete                  |");
		System.out.println("|5 - back                    |");
		System.out.println("|----------------------------|");
		
		int keyInt = keyPressed.nextInt();

		
	switch(keyInt){
	
	
	case 0:
	
		
	GenericFunction.printArtworkID(MySqlArtworkRepository.getAllId());
	System.out.println("Press Any Key To Continue...");
    new java.util.Scanner(System.in).nextLine();
    statueMenu();
	break;
	
	
	case 1:

		System.out.println("View statue: insert ID...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			GenericFunction.printArtwork(MySqlArtworkRepository.getById(keyPressed2.nextLine()));

		statueMenu();
		break;
		}
	case 2:
		
		System.out.println("Inserti statue");
		MySqlArtworkRepository.addArtwork(GenericFunction.insertArtwork());
		System.out.println("insert OK");
		statueMenu();
		break;	
		
	case 3:
		System.out.println("Update statue: insert ID...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			
			while(keyPressed2.hasNext()){
				keyPressedString = keyPressed2.nextLine();
				MySqlArtworkRepository.updateArtwork(GenericFunction.updateArtwork(), keyPressedString);
		
			}
		}
		statueMenu();
		break;
	
	case 4:
		String id;
		System.out.println("Delete statue: insert ID...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			
			id =keyPressed2.nextLine();
			
				MySqlArtworkRepository.deleteArtwork(id);		
			}
		
		statueMenu();

		break;

	case 5:
		categoryMenu();
		break;
		
	default:
		System.out.println("Choose correct Artwork...");
		break;
	}
	
	}while(keyPressed.hasNextInt());	
	}
}

static public void buildingMenu(){
	
	String keyPressedString;
	
	try (Scanner keyPressed = new Scanner(System.in)){

	
	do{
	
		System.out.println("|--------BUILDING  MENU------|"); //istruzione per stampare a video 
		System.out.println("|----------------------------|");
		System.out.println("|0 - List                    |"); //istruzione per stampare a video 
		System.out.println("|1 - View                    |");
		System.out.println("|2 - Insert                  |");
		System.out.println("|3 - Update                  |");
		System.out.println("|4 - Delete                  |");
		System.out.println("|5 - back                    |");
		System.out.println("|----------------------------|");
		
		int keyInt = keyPressed.nextInt();

		
	switch(keyInt){
	
	
	case 0:
	
		
	GenericFunction.printArtworkID(MySqlArtworkRepository.getAllId());
	System.out.println("Press Any Key To Continue...");
    new java.util.Scanner(System.in).nextLine();
    statueMenu();
	break;
	
	
	case 1:

		System.out.println("Insert ID building...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
		while(keyPressed2.nextLine()!=null){
			GenericFunction.printArtwork(MySqlArtworkRepository.getById(keyPressed2.nextLine()));
		}
		buildingMenu();
		break;
		}
	case 2:
		
		System.out.println("Insert Building");
		MySqlArtworkRepository.addArtwork(GenericFunction.insertArtwork());
		System.out.println("Insert OK");
		buildingMenu();
		break;	
		
	case 3:
		System.out.println("Update building: insert ID");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			
			while(keyPressed2.hasNext()){
				keyPressedString = keyPressed2.nextLine();
				MySqlArtworkRepository.updateArtwork(GenericFunction.updateArtwork(), keyPressedString);
			}
		}
		buildingMenu();
		break;
	
	case 4:
		String id;
		System.out.println("Delete Building: insert ID...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			
			id =keyPressed2.nextLine();
			
				MySqlArtworkRepository.deleteArtwork(id);		
			}
		
		statueMenu();

		break;

	case 5:
		categoryMenu();
		break;
		
	default:
		System.out.println("Choose correct Artwork...");
		break;
	}
	
	}while(keyPressed.hasNextInt());	
	}
}

static public void paintingMenu(){
	
	String keyPressedString;
	
	try (Scanner keyPressed = new Scanner(System.in)){

	
	do{
	
		System.out.println("|--------PAINTING  MENU------|"); //istruzione per stampare a video 
		System.out.println("|----------------------------|");
		System.out.println("|0 - List                    |"); //istruzione per stampare a video 
		System.out.println("|1 - View                    |");
		System.out.println("|2 - Insert                  |");
		System.out.println("|3 - Update                  |");
		System.out.println("|4 - Delete                  |");
		System.out.println("|5 - back                    |");
		System.out.println("|----------------------------|");
		
		int keyInt = keyPressed.nextInt();

		
	switch(keyInt){
	
	
	case 0:
	
		
	GenericFunction.printArtworkID(MySqlArtworkRepository.getAllId());
	System.out.println("Press Any Key To Continue...");
    new java.util.Scanner(System.in).nextLine();
    statueMenu();
	break;
	
	
	case 1:

		System.out.println("Insert ID painting...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
		while(keyPressed2.nextLine()!=null){
			GenericFunction.printArtwork(MySqlArtworkRepository.getById(keyPressed2.nextLine()));
		}
		buildingMenu();
		break;
		}
	case 2:
		
		System.out.println("Insert painting");
		MySqlArtworkRepository.addArtwork(GenericFunction.insertArtwork());
		System.out.println("Insert OK");
		buildingMenu();
		break;	
		
	case 3:
		System.out.println("Update painting: insert ID");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			
			while(keyPressed2.hasNext()){
				keyPressedString = keyPressed2.nextLine();
				MySqlArtworkRepository.updateArtwork(GenericFunction.updateArtwork(), keyPressedString);
			}
		}
		paintingMenu();
		break;
	
	case 4:
		String id;
		System.out.println("Delete painting: insert ID...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			
			id =keyPressed2.nextLine();
			
				MySqlArtworkRepository.deleteArtwork(id);		
			}
		
		paintingMenu();

		break;

	case 5:
		categoryMenu();
		break;
		
	default:
		System.out.println("Choose correct Artwork...");
		break;
	}
	
	}while(keyPressed.hasNextInt());	
	}
}

static public void artefactMenu(){
	
	String keyPressedString;
	
	try (Scanner keyPressed = new Scanner(System.in)){

	
	do{
	
		System.out.println("|--------ARTEFACT  MENU------|"); //istruzione per stampare a video 
		System.out.println("|----------------------------|");
		System.out.println("|0 - List                    |"); //istruzione per stampare a video 
		System.out.println("|1 - View                    |");
		System.out.println("|2 - Insert                  |");
		System.out.println("|3 - Update                  |");
		System.out.println("|4 - Delete                  |");
		System.out.println("|5 - back                    |");
		System.out.println("|----------------------------|");
		
		int keyInt = keyPressed.nextInt();

		
	switch(keyInt){
	
	
	case 0:
	
		
	GenericFunction.printArtworkID(MySqlArtworkRepository.getAllId());
	System.out.println("Press Any Key To Continue...");
    new java.util.Scanner(System.in).nextLine();
    statueMenu();
	break;
	
	
	case 1:

		System.out.println("Insert ID building...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
		while(keyPressed2.nextLine()!=null){
			GenericFunction.printArtwork(MySqlArtworkRepository.getById(keyPressed2.nextLine()));
		}
		artefactMenu();
		break;
		}
	case 2:
		
		System.out.println("Insert Artefact");
		MySqlArtworkRepository.addArtwork(GenericFunction.insertArtwork());
		System.out.println("Insert OK");
		artefactMenu();
		break;	
		
	case 3:
		System.out.println("Update building: insert ID");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			
			while(keyPressed2.hasNext()){
				keyPressedString = keyPressed2.nextLine();
				MySqlArtworkRepository.updateArtwork(GenericFunction.updateArtwork(), keyPressedString);
			}
		}
		artefactMenu();
		break;
	
	case 4:
		String id;
		System.out.println("Delete Building: insert ID...");
		
		try (Scanner keyPressed2 = new Scanner(System.in)){
			
			id =keyPressed2.nextLine();
			
				MySqlArtworkRepository.deleteArtwork(id);		
			}
		
		artefactMenu();

		break;

	case 5:
		categoryMenu();
		break;
		
	default:
		System.out.println("Choose correct Artwork...");
		break;
	}
	
	}while(keyPressed.hasNextInt());	
	}
}

}

