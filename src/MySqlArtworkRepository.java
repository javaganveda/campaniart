import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MySqlArtworkRepository {
	String sql;
	private static final String URL = "jdbc:mysql://127.0.0.1:3306/campaniart";
	private static final String USER = "root";
	private static final String PASSWORD = "Protom2016";
	
	public static void addArtwork(Artwork artwork) {
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "INSERT INTO artwork(id,name, age, city, address, civicnumber,xsat,ysat,idartist, value, tipology, height, width , weight, lenght, description ,idphoto, deleted) " + 
					"VALUES ('%s', '%s', %d, '%s','%s',%d, %f,%f,'%s',%f,%d,%f,%f,%f,%f, '%s', '%s'  , %b)",
					artwork.getId(),artwork.getName(),artwork.getAge(), artwork.getCity(),artwork.getAddress(),artwork.getCivicNumber(),
					artwork.getxSat(),artwork.getySat(),artwork.getIdArtist(),artwork.getValue(),artwork.getTipology(), artwork.getHeight(),
					artwork.getWidth(),artwork.getWeight(),artwork.getLenght(), artwork.getDescription(),artwork.getIdPhoto(), artwork.isDeleted());
			
			/*
			 String sql = String.format(Locale.ENGLISH, "INSERT INTO heroes(name, genre, life, power, resistence , typeElement, fire, water , earth, air, coin) " + 
										  "VALUES ('%s', %b, %d,%d,%f,%d,%d,%d,%d,%d,%d)", hero.getName(),hero.isGenre(),hero.getLife(),hero.getPower(),
										  hero.getResistence(),hero.getTypeElement(),hero.getElement()[0],hero.getElement()[1],hero.getElement()[2],
										  hero.getElement()[3],hero.getElement()[4]);
			 */
			
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE ADD
	
	public static void updateArtwork(Artwork artwork,String id) {
		String sql;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `campaniart`.`artwork` SET `name` = '%s',"
					+ "`age` = %d,"
					+ "`city` = '%s',"
					+ "`address` = '%s',"
					+ "`civicnumber` = %d,"
					+ "`xsat` = %f,"
					+ "`ysat` = %f,"
					+ "`idartist` = '%s',"
					+ "`value` = %f,"
					+ "`tipology` = %d,"
					+ "`height` = %f,"
					+ "`width` = %f,"
					+ "`weight` = %f,"
					+ "`lenght` = %f,"
					+ "`description` = '%s',"
					+ "`idphoto` = '%s' "
					+ "WHERE `id` = '%s';",
					artwork.getName(),artwork.getAge(),artwork.getCity(),
					artwork.getAddress(),artwork.getCivicNumber(),artwork.getxSat(),artwork.getySat(),artwork.getIdArtist(),
					artwork.getValue(),artwork.getTipology(),artwork.getHeight(),artwork.getWidth(),artwork.getWeight(),artwork.getLenght(),
					artwork.getDescription(),artwork.getIdPhoto(),id);
			/*UPDATE `campaniart`.`artwork` SET `name` = 'gggg',`age` = 4,`city` = 'frr',`address` = 'rf',`civicnumber` = 4,`xsat` = 4.000000,`ysat` = 4.000000,`idartist` = 'yyh' ,`value` = 4.000000,`tipology` = 1,`height` = 2.000000,`width` = 2.000000,`weight` = 2.000000,`lenght` = 2.000000,`description` = 'ggh',`idphoto` = 'ede' WHERE `id` = 'gio';

*/
			System.out.println(sql);
 
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE UPDATE
	
	public static void deleteArtwork(String id) {
		String sql;
		//UPDATE `campaniart`.`artwork` SET `deleted` = 1 WHERE `id` = 'gio';
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "UPDATE `campaniart`.`artwork` SET `deleted` = %d "
					+ "WHERE `id` = '%s';",
					1,id);
					 System.out.println(sql);
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE DELETE
	
	
	public void create_schema(){
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE SCHEMA `campaniart`";
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create schema
	
	public void create_table(){
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `campaniart`.`artwork`(`id` VARCHAR(45) NOT NULL,`name` VARCHAR(45),`age` INT NULL,`value` DOUBLE NULL,"
					+ "`idposition` VARCHAR(45) NULL,"
					+ "`idartist` VARCHAR(45) NULL,"
					+ "`height` DOUBLE NULL,"
					+ "`width` DOUBLE NULL,"
					+ "`weight` DOUBLE NULL,"
					+ "`lenght` DOUBLE NULL,"
					+ "`description` VARCHAR(100) NULL,"
					+ "`deleted` TINYINT,"
					+ "PRIMARY KEY (`id`))";

			statement.executeUpdate(sql);
		
			sql = "CREATE TABLE `sfogliart`.`artist`(`idartist` VARCHAR(45) NOT NULL,`firstname` VARCHAR(45),`lastname` VARCHAR(45),`birthyear` INT NOT NULL,"
					+ "`idstatue` VARCHAR(45) NULL,"
					+ "PRIMARY KEY (`idartist`),"
					+ "FOREIGN KEY (`idstatue`) REFERENCES `statue`(`id`))";
			

			statement.executeUpdate(sql);
			/*
			CREATE TABLE `sfogliart`.`new_table2` (
					  `idprova` INT NOT NULL,
					  `nameprova` VARCHAR(45) NULL,
					  PRIMARY KEY (`idprova`),
					  CONSTRAINT `id`
					    FOREIGN KEY (`idprova`)
					    REFERENCES `sfogliart`.`new_table` (`age`)
					    ON DELETE NO ACTION
					    ON UPDATE NO ACTION);

			*/

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	}//fine create table
	
	/*OBSOLETO
	public void delete(String id){
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			//sql = String.format("DELETE FROM statue WHERE id = %s", id);

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			System.out.println(sql);
			e.printStackTrace();
			}
	}
	*/
	
	public List<Artwork> getAll() {
		List<Artwork> artworkList = new ArrayList<Artwork>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM artwork");

			while (resultSet.next()) {
				Artwork artworks = new Artwork();
				artworks.setId(resultSet.getString("id"));
				artworks.setId(resultSet.getString("name"));
				artworks.setAge(resultSet.getInt("age"));
				artworks.setValue(resultSet.getDouble("value"));
				artworks.setHeight(resultSet.getDouble("height"));
				artworks.setWidth(resultSet.getDouble("width"));
				artworks.setWeight(resultSet.getDouble("weight"));
				artworks.setLenght(resultSet.getDouble("lenght"));

				artworkList.add(artworks);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return artworkList;
	}//fine get all
	
	
	public static List<Artwork> getAllId() {
		List<Artwork> artworkIdList = new ArrayList<Artwork>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM artwork");

			while (resultSet.next()) {
				Artwork artworks = new Artwork();
				artworks.setId(resultSet.getString("id"));
				artworkIdList.add(artworks);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return artworkIdList;
	}//fine get all
	
	public static List<Artwork> getById(String id) {
		String sql;
		Artwork artwork = null;
		List<Artwork> artworkIdList = new ArrayList<Artwork>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format("SELECT * FROM artwork c WHERE c.id = '%s'", id);
			
			ResultSet resultSet = statement.executeQuery(sql);
			
			if (resultSet.first()) {
				Artwork artworks = new Artwork();
				artworks.setId(resultSet.getString("id"));
				artworks.setId(resultSet.getString("name"));
				artworks.setAge(resultSet.getInt("age"));
				artworks.setId(resultSet.getString("city"));
				artworks.setId(resultSet.getString("address"));
				artworks.setAge(resultSet.getInt("civicnumber"));
				artworks.setValue(resultSet.getDouble("xsat"));
				artworks.setHeight(resultSet.getDouble("ysat"));
				artworks.setId(resultSet.getString("idArtist"));
				artworks.setValue(resultSet.getDouble("value"));
				artworks.setAge(resultSet.getInt("tipology"));
				artworks.setHeight(resultSet.getDouble("height"));
				artworks.setWidth(resultSet.getDouble("width"));
				artworks.setWeight(resultSet.getDouble("weight"));
				artworks.setLenght(resultSet.getDouble("lenght"));
				artworks.setId(resultSet.getString("description"));
				artworks.setId(resultSet.getString("idphoto"));


				artworkIdList.add(artworks);
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return artworkIdList;
	}
	
	
	
	
	
}//fine classe
