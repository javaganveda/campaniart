
	import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
	import java.util.List;
	import java.util.Scanner;

	public class GenericFunction {

		
		public static Artwork insertArtwork(){
			Scanner keyPressed = new Scanner(System.in);
			String keyString;
			int keyNumberInt;
			double keyNumberDecimal;

			Artwork artwork = new Artwork();
			
			//inizio inserimento
			System.out.println("insert a new artwork");
			System.out.println("Insert id:");
			keyString = keyPressed.nextLine();
			artwork.setId(keyString);

			System.out.println("Insert Name:");
			keyString = keyPressed.nextLine();
			artwork.setName(keyString);
			
			System.out.println("Insert Age:");
			keyNumberInt = keyPressed.nextInt();
			artwork.setAge(keyNumberInt);			
			keyString = keyPressed.nextLine();


			System.out.println("Insert City:");
			keyString = keyPressed.nextLine();
			artwork.setCity(keyString);
			keyString = keyPressed.nextLine();


			System.out.println("Insert address:");
			keyString = keyPressed.nextLine();
			artwork.setAddress(keyString);
			keyString = keyPressed.nextLine();

			System.out.println("Insert civic number:");
			keyNumberInt = keyPressed.nextInt();
			artwork.setCivicNumber(keyNumberInt);
			
			System.out.println("Insert Sat X Cordinate:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setxSat(keyNumberDecimal);
			
			System.out.println("Insert Sat Y Cordinate:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setySat(keyNumberDecimal);
			keyString = keyPressed.nextLine();

			
			System.out.println("Insert ID artist:");
			keyString = keyPressed.nextLine();
			artwork.setIdArtist(keyString);
			keyString = keyPressed.nextLine();
			
			System.out.println("Insert Value:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setValue(keyNumberDecimal);

			System.out.println("Insert tipology:");
			System.out.println("1 - Statue");
			System.out.println("2 - Painting");
			System.out.println("3 - Building");
			System.out.println("4 - Artfact");
			System.out.println("-------------");

			keyNumberInt = keyPressed.nextInt();
			artwork.setTipology(keyNumberInt);
			
			System.out.println("Insert Height:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setHeight(keyNumberDecimal);
			
			System.out.println("Insert Width:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setWidth(keyNumberDecimal);
			
			System.out.println("Insert Weight:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setWeight(keyNumberDecimal);
			
			System.out.println("Insert Lenght:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setLenght(keyNumberDecimal);
			
			System.out.println("Insert Description:");
			keyString = keyPressed.nextLine();
			artwork.setDescription(keyString);
			keyString = keyPressed.nextLine();
			
			System.out.println("Insert idPhoto:");
			keyString = keyPressed.nextLine();
			artwork.setIdPhoto(keyString);
			
			artwork.setDeleted(false);
			
			return artwork;	
		}
			
		public static Artwork updateArtwork(){
			Scanner keyPressed = new Scanner(System.in);
			String keyString;
			int keyNumberInt;
			double keyNumberDecimal;

			Artwork artwork = new Artwork();
			
			System.out.println("Insert Name:");
			keyString = keyPressed.nextLine();
			artwork.setName(keyString);
			
			System.out.println("Insert Age:");
			keyNumberInt = keyPressed.nextInt();
			artwork.setAge(keyNumberInt);	
			keyPressed.nextLine();
			System.out.println("Insert City:");
			keyString = keyPressed.nextLine();
			artwork.setCity(keyString);


			System.out.println("Insert address:");
			keyString = keyPressed.nextLine();
			artwork.setAddress(keyString);

			System.out.println("Insert civic number:");
			keyNumberInt = keyPressed.nextInt();
			artwork.setCivicNumber(keyNumberInt);
			
			System.out.println("Insert Sat X Cordinate:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setxSat(keyNumberDecimal);
			
			System.out.println("Insert Sat Y Cordinate:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setySat(keyNumberDecimal);
			keyPressed.nextLine();

			System.out.println("Insert ID artist:");
			keyString = keyPressed.nextLine();
			artwork.setIdArtist(keyString);
			
			System.out.println("Insert Value:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setValue(keyNumberDecimal);

			System.out.println("Insert tipology:");
			System.out.println("1 - Statue");
			System.out.println("2 - Painting");
			System.out.println("3 - Building");
			System.out.println("4 - Artfact");
			System.out.println("-------------");

			keyNumberInt = keyPressed.nextInt();
			artwork.setTipology(keyNumberInt);
			
			System.out.println("Insert Height:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setHeight(keyNumberDecimal);
			
			System.out.println("Insert Width:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setWidth(keyNumberDecimal);
			
			System.out.println("Insert Weight:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setWeight(keyNumberDecimal);
			
			System.out.println("Insert Lenght:");
			keyNumberDecimal = keyPressed.nextDouble();
			artwork.setLenght(keyNumberDecimal);
			keyPressed.nextLine();

			System.out.println("Insert Description:");
			keyString = keyPressed.nextLine();
			artwork.setDescription(keyString);
			
			System.out.println("Insert idPhoto:");
			keyString = keyPressed.nextLine();
			artwork.setIdPhoto(keyString);
			
			artwork.setDeleted(false);
			
			return artwork;	
		}
		
		
		
		public static void printArtworkID (List<Artwork> listArtwork){
			for(Artwork listArt : listArtwork) {
				System.out.println("|Id:\t" + listArt.getId() + "\n" + "|Name:\t" + listArt.getName() + "\n" + "--------------" + "\n");
			}
		}
		
		public static void printArtwork (List<Artwork> listArtwork){
			for(Artwork listArt : listArtwork) {
				System.out.println("|Id:\t" + listArt.getId() + "\n" +
			"|Name:\t" + listArt.getName() + "\n" +	
			"|Age:\t" + listArt.getAge() + "\n" +
			"|City:\t" + listArt.getCity() + "\n" +
			"|Address:\t" + listArt.getAge() + " , " + listArt.getCivicNumber() + "\n" +
			"|Sat Coordinates:\t" + "X: " + listArt.getxSat() + " - " + "Y:  " +  listArt.getySat() + "\n" +
			"|ID Artist:\t" + listArt.getIdArtist() + "\n" +
			"|Value:\t" + listArt.getValue() + "\n" +
			"|Tipology:\t" + printTipology(listArt.getTipology()) + "\n" +
			"|Value:\t"  + listArt.getValue() + "\n"+ 
			"|Lenght:" + listArt.getLenght() + " Weight:" + listArt.getWeight() +
			" Widht:" + listArt.getWidth() + " Height:" + listArt.getHeight() + "\n" +
			"|Description:\t" + listArt.getDescription() + "\n");
			}
			
			System.out.println("Press Any Key To Continue...");
	          new java.util.Scanner(System.in).nextLine();
		}
		
		static String printTipology(int tipology){
			String type = null;
			
			switch(tipology){
			
			case 1:
				type = "Statue";
				break;
			case 2:
				type = "Painting";
				break;
			case 3:
				type = "Building";
				break;
			case 4:
				type = "manufact";
				break;
			}
			
			return type;
		}
}
